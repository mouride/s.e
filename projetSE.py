#!/usr/bin/env/python
# -*- coding: iso8859-1 -*-

import os,sys
from math import sqrt
#import jeton

Size = 10

def decode(s):
    l = len(s)
    return (s[0], s[1:(l-2)])    
    
def broken():
    return "J;\n"

def prime(n):
    return ("P"+str(n)+";\n")

def noprime(n):
    return ("N"+str(n)+";\n")
    
def isPrime(n):
    if(n<2):
        return False
    i = 2
    while(i<=sqrt(n)):
        if(n%i==0):
            return False
        else:
            i = i + 1
    return True

def laste(l):
    c = len(l)
    return ", last=" + str(l[c-1])

def liste(l):
    return l

def bilan(n):
    return str(n) + " primes "


if __name__== "__main__":
    
    msg = []
    jeton = True
    nb = 0
    """
    if(sys.argv < 2):
        print("Usage : n_station")
        sys.exit(1)
        
    this_fils = sys.argv[1]
    
    if(this_fils==0):
        mes = "jeton libre :\n"
        os.write(1,mes)
    """    
    while(1):
        m = os.read(0,Size)
        r = decode(m)
        #r_int = int(r[1])
        #r_float = float(r[1])
        if(r[0]=="J"): # saisi = jeton
            jeton = False
        elif((r[0]=="P") & (not jeton)): # saisi = Pn et jeton non libre;
            msg.append(int(r[1]))
            nb += 1
            jeton = True
            os.write(1,broken())
        elif((r[0]=="P") & jeton): # saisi = Pn et jeton libre
            os.write(1,m)
        elif(r[0]=="N"): # non premier
            os.write(1,m)
        elif(r[0]=="?"):
            s = str(isPrime(int(r[1])))+"\n"
            os.write(1,s)
        elif((r[0]=="S") & (not len(msg)==0)):
            b = bilan(nb) + laste(msg) +"\n"
            os.write(1,b)
        elif((r[0]=="S") & len(msg)==0):
            tm = str(msg)+"\n"
            os.write(1,tm)
        elif(r[0]=="D"):
            tm = str(msg)+"\n"
            os.write(1,tm)
    